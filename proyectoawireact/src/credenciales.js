// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import 'firebase/compat/storage';
import 'firebase/compat/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDkxlykdGZ8503szHOqX6QTTJzQ67KYW8Y",
  authDomain: "trainings-82cda.firebaseapp.com",
  projectId: "trainings-82cda",
  storageBucket: "trainings-82cda.appspot.com",
  messagingSenderId: "909699424447",
  appId: "1:909699424447:web:4b131a386bdd29fe89e60a"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

export default firebaseApp;