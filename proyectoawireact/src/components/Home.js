import React from 'react';
import firebaseApp from '../credenciales';
import { getAuth, signOut } from 'firebase/auth';
import { Link } from 'react-router-dom';
const auth = getAuth(firebaseApp);

const Home = () => {
    
    return <div class="body-home">
        <div class="main-index">
        <div class="inicial">
            <nav class="nav-inicial">
                <ul class="ul-inicial">
                    <li><a href="quienessomos.html">¿Quiénes somos?</a></li>
                    <button type="submit" class="btn-logout" onClick={() => signOut(auth)}>Cerrar Sesión</button>
                </ul>
            </nav>
        </div>
        <div class="second">
            <div class="img-main"></div>
            <h2>Empieza tus Capacitaciones hoy mismo de entre los múltiples cursos que tenemos para ti</h2>
        </div>
        <div class="nav-menu">
            <nav>
                <ul>
                    <li><a href="http://127.0.0.1:5501/TrabajoGrupal_Avances/main.html">Inicio</a></li>
                    <li><a href="http://127.0.0.1:5501/TrabajoGrupal_Avances/cursos.html" class="Perifericos">Vista de Cursos</a></li>
                    <li><a href="http://127.0.0.1:5501/TrabajoGrupal_Avances/comunity.html">Comunidad</a></li>
                    <li><a href="http://127.0.0.1:5501/TrabajoGrupal_Avances/articulos.html" target="_blank" class="celulares">Artículos</a></li>
                    <li><a href="http://127.0.0.1:5501/TrabajoGrupal_Avances/profilee.html">Mi perfil</a></li>
                    
                </ul>
            </nav>
        </div>
        <div class="imag tird">
            <h2>"La tecnología hizo posible las grandes poblaciones; ahora las grandes poblaciones hacen que la tecnología sea indispensable." <br/> <span>José Krutch</span>
            </h2>
        </div>
        <section class="detail">
            <div class="box-one">
                <a href="#">Se parte hoy de quienes han decidido transformar sus vidas</a >
            </div>
            <div class="box-two">
                <a href="#">Estudia nuestros cursos y conviertete en un profesional de las tecnologías que mueven el mundo</a >
            </div>
            <div class="box-three">
                <a href="#">Estudia desde el lugar donde te encuentres y continúa con tu aprendizaje</a >
            </div>
        </section>
        <div class="separador">
            <h4>Nuevos cursos</h4>
            <hr/>
        </div>
        <section class="news-prod">
            <div class="sbox-one">
                <a href="CursoCSS.html">CSS de noob a pro</a >
            </div>
            <div class="sbox-two">
                <a href="#">Comunicaciones y redes</a >
            </div>
            <div class="sbox-three">
                <a href="#">React Native Avanzado</a >
            </div>
            <div class="sbox-four">
                <a href="#">Java Nivel Básico   </a >
            </div>
        </section>

        <footer>
            Derechos Reservados - 2021
        </footer>        
        </div>
    </div>
}

export default Home
