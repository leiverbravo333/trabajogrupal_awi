import React, { useState } from 'react';
import '../assets/css/styles.css';
import firebaseApp from '../credenciales.js';
import { 
    getAuth, 
    createUserWithEmailAndPassword, 
    signInWithEmailAndPassword,
    signInWithRedirect,
    GoogleAuthProvider
} from 'firebase/auth';
const auth = getAuth(firebaseApp);
const gooPro = new GoogleAuthProvider();

const Logueo = () => {
    const [isRegistering, setIsRegistering] = useState(false);
    async function submitHandler(e) {
        e.preventDefault();
        const correo = e.target.userid.value;
        const clave = e.target.id_control.value;

        if(isRegistering){
            const usuario = await createUserWithEmailAndPassword(auth, correo, clave);
        }else {
            signInWithEmailAndPassword(auth, correo, clave);
        }
    }


    return (
        <container class="body-login">
            <stack gap={3}>
                <header class="header">
                    <div class="cont-image-main"><div class="image-main"></div></div>
                    <h1>Trainings&</h1>
                    <h2>Capacitaciones Online sobre tecnología</h2>
                </header>

                <div class="container">
                    <h3>{isRegistering ? "Registrate" : "Inicia Sesión"} en Training&</h3>
                    <form onSubmit={submitHandler} id="formulario">
                            <label for="userid" class="datos">{isRegistering ? "Correo electrónico:" : "Usuario:"}</label>
                            <input type="text" class="input-main" name="name_control" id="userid" autofocus required />
                            <label for="id_control" class="datos">Contraseña:</label>
                            <input type="password" class="input-main" name="ctr_control" id="id_control" autofocus required />
                            <button type="submit" class="boton">
                                {isRegistering ? "Regístrate" : "Inicia Sesión"}
                            </button>
                    </form>
                </div>
                <input type="submit" value="Acceder con Google" class="boton google-btn" onClick={() => signInWithRedirect(auth, gooPro)}/>
                <button type="submit" class="boton" onClick={() => setIsRegistering(!isRegistering)}>
                    {isRegistering ? "¿Ya tienes cuenta? Inicia Sesión" : "¿Aún no te has registrado? Regístrate"}
                </button>
            </stack>
        </container>
    );
}

export default Logueo
