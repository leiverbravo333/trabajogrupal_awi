// Your web app's Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyAvTBYUu0meiQcSo_EFzfaHWktoPSyRzdg",
      authDomain: "trainings-cursos.firebaseapp.com",
      projectId: "trainings-cursos",
      storageBucket: "trainings-cursos.appspot.com",
      messagingSenderId: "225706866204",
      appId: "1:225706866204:web:b87f55b8bbf907868b11d9"
    };
  
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    let db = firebase.firestore();

    const SaveUser = (user) => {
    db.collection("usuarios").add({
        user
    })
    .then(function(docRef) {
        MJSOK();
    })
    .catch(function(error) {
        MJSERROR();
    })
}

const MJSOK =()=>{
    Swal.fire(
        'Good job!',
        'Datos guardados correctamente!',
        'success'
      )
}

const MJSERROR =()=>{
    Swal.fire(
        'Good job!',
        'Datos no guardados',
        'error'
      )
}

$("#boton-register").on('click', ()=>{
    let nombre = $("#nameid").val();
    let apellido = $("#lastnameid").val();
    let fechaNacimiento = $("#fechaid").val();
    let email = $("#emailid").val();
    let contrasenia = $("#contralid").val();

    const user = {
        nombre,
        apellido,
        fechaNacimiento,
        email,
        contrasenia
    }

    SaveUser(user);
})