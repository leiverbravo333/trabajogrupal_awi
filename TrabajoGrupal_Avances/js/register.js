//VERIFICACIÓN DE COINCIDENCIA DE CONTRASEÑAS DEL REGISTRO
document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("form").addEventListener('submit', verificarPasswords); 
  });

function verificarPasswords() {
 
    // Ontenemos los valores de los campos de contraseñas 
    contralid = document.getElementById('contralid');
    cofcontralid = document.getElementById('cofcontralid');
 
    // Verificamos si las constraseñas no coinciden 
    if (contralid.value != cofcontralid.value) {
 
        // Si las constraseñas no coinciden mostramos un mensaje 
        alert("Las contraseñas no coinciden");return false;
    } else {
   
        
        // Refrescamos la página (Simulación de envío del formulario) 
        setTimeout(function() {
            location.reload();
        }, 3000);
 
        return true;
    }
 
}
