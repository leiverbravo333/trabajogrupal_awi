//Declaración de variables 
var color_fondo = ["#0D1120", "#B8B8B8"];


// Declaración de Funciones
function recuperar(){
    alert("¡Se ha enviado un correo para recuperar su contraseña!");
    alert("Se ha enviado un correo electrónico de restablecimiento de contraseña a la dirección de correo electrónico de tu cuenta, pero puede llevar varios minutos que aparezca en tu bandeja de entrada.")
}
function pago(){
    alert("!Su pago fue exitoso!")
}


let botones = document.querySelectorAll(".body-form-center-btn");

botones.forEach((element) => {
  element.addEventListener("click", () => {
    let input = element.previousElementSibling;

    if (input.hasAttribute("readonly")) {
      input.removeAttribute("readonly");
      element.innerHTML = `<i class="ion-md-checkmark-circle-outline"></i>`;
    } else {
      input.setAttribute("readonly", true);
      element.innerHTML = `<i class="ion-md-create"></i>`;
    }
  });
});


//Función para verificar los datos del login
var usuario = document.getElementById("userid");
var password = document.getElementById("id_control");

document.addEventListener("DOMContentLoaded", function() {
  document.getElementById("formulario").addEventListener('submit', validarFormulario); 
  });
  
function validarFormulario() {
  var clave = document.getElementById('id_control').value;
  var user = document.getElementById('userid').value;

  if(clave.length <= 4 && user.length <= 5) {
    alert('El usuario debe contener mínimo 5 caracteres y la contraseña mínimo tener 4 caracteres');
    return;
  }
  else{
        return alert("Bienvenido de vuelta! Estudia ahora con Training&");
    }
  }

  
function success() {
  if(usuario.value === null || usuario.value === ''){
    alert("Ingresa tus datos");
  } else if(password.value === null || password.value === ''){
    alert("Ingresa tus datos");
    }else{
      validarFormulario();
    }
}



  //Funcion de Notificar
function notifyMe() {

  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }


  else if (Notification.permission === "granted") {

    var notification = new Notification("Hi there!");
  }


  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
 
      if (permission === "granted") {
        var notification = new Notification("Hi there!");
      }
    });
  }


}

Notification.requestPermission().then(function(result) {
  console.log(result);
});
function spawnNotification(theBody,theIcon,theTitle) {
  var options = {
      body: theBody,
      icon: theIcon
  }
  var n = new Notification(theTitle,options);
}

//comunidad
document.addEventListener("DOMContentLoaded", function(e){
  const parrafos = document.querySelectorAll('.parrafo-post');
  let alturas = [];
  let alturaMaxima = 0;

  const AplicarAlturas = (function AplicarAlturas (){
    parrafos.forEach(parrafo => {
      if(alturaMaxima == 0){
        alturas.push(parrafo.clientHeight);
      }else{
        parrafo.style.height = alturaMaxima + "px";
      }
    });
    return AplicarAlturas;
  })();
  alturaMaxima = Math.max.apply(Math, alturas);
  AplicarAlturas();
});

