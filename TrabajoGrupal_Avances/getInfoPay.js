// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAvTBYUu0meiQcSo_EFzfaHWktoPSyRzdg",
    authDomain: "trainings-cursos.firebaseapp.com",
    projectId: "trainings-cursos",
    storageBucket: "trainings-cursos.appspot.com",
    messagingSenderId: "225706866204",
    appId: "1:225706866204:web:b87f55b8bbf907868b11d9"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  let db = firebase.firestore();
  const SaveData = (paydate) => {
    db.collection("datospago").add({
        paydate

    })
    .then(function(docRef) {
        MJSOK();
    })
    .catch(function(error) {
        MJSERROR();
    })
  }
  
  const MJSOK =()=>{
    Swal.fire(
        'Good job!',
        'Pago realizado con éxito',
        'success'
      )
  }
  
  const MJSERROR =()=>{
    Swal.fire(
        'Good job!',
        'Datos no validos',
        'error'
      )
  }
  
  $("#boton_pago").on('click', ()=>{
    let NumeroTarjeta = $("#tarjeta_id").val();
    let NombreTitular = $("#titular_control").val();
    let Fecha = $("#date_control").val();
    let CodidoVV = $("#cvv_control").val();
  
  
    const paydate = {
        NumeroTarjeta,
        NombreTitular,
        Fecha,
        CodigoVV
    }
  
    SaveData(paydate);
  })